$(function() {

    /*Getting DOM*/
    var height = $(window).height();
    var width = $(window).width();
    var body = $('body');
    var pageId = body.attr("id");

    var logo = $('#logo');
    var logoPath = $('#logoPath');
    var logoPathNaked = $('#logoPathNaked');
    var logoPathFeen = $('#logoPathFeen');
    var nav = $('nav li');
    var feen = $('#feen');
    var enButton = $('#langs');
    var lyrics = $('.lyrics');
    var lyricsH = $('.lyrics h3');

    var ralph = $('#Ralph');
    var volker = $('#Volker');
    var matze = $('#Matze');
    var markus = $('#Markus');
    var feenPath = $('#feenPath');
    var tooltip = $("#tooltip");
    var triangle = $("#triangle");

    /*ADJUSTMENTS*/
    var background_colors = new Array("#B33634", "#E66526", "#66BC68", "#336B80", "#EAAB1A");
    /*Red #B33634
    Red2 #B02D39
    
    
    */
    var bounceDuration = 2000;
    var bounceHeight = -300;
    var bounceDepth = -600;
    var navFlyInSpeed = 150;

    /*SETTING THE BACKGROUND-COLOR*/
    body.css('background-color', background_colors[getRandomInt(0, background_colors.length - 1)]);
    feenPath.css('fill', '#FFFFFF');

    /*CLICK and HOVER*/
    //logo.hover
    logoPath.mouseover(function() {
        logoPathFeen.css('fill', '#FFFFFF');
        logoPathNaked.css('fill', '#000000');
    }).mouseout(function() {
        logoPathFeen.css('fill', '#000000');
        logoPathNaked.css('fill', '#FFFFFF');
    });
    //feen.hover
    tooltipHover("#Ralph");
    tooltipHover("#Volker");
    tooltipHover("#Matze");
    tooltipHover("#Markus");
    tooltip.css('zIndex', 9999);
    triangle.css('zIndex', 9999);
    function tooltipHover(id) {
        $(id).css('fill', '#000000');
        $(id).mousemove(function(e) {
            var x = mouseX(e) - tooltip.width();
            var y = mouseY(e) - tooltip.height() - 40;

            tooltip.find('p').html($(id).attr("title"));
            tooltip.css({
                'left' : x,
                'top' : y
            });
            triangle.css({
                'left' : x + tooltip.width(),
                'top' : y + (2 * tooltip.height() - 1)
            });
            $(id).css('fill', '#FFFFFF');
            tooltip.show();
            triangle.show();
        }).mouseout(function() {
            $(id).css('fill', '#000000');
            triangle.hide();
            tooltip.hide();
        });
    };
    //enButton.click and hover
    enButton.mouseover(function() {
        enButton.css('fill', '#000000');
    });
    lyricsH.bind('click', function() {
        if ($(this).next().is(':visible')) {
            $(this).next().hide(500);
        } else {
            $(this).next().show(500);
        }
    });

    /*HOME-PAGE*/
    if (pageId == "home") {

        /*setup*/
        //Hide logo
        logo.css("top", bounceDepth);
        logo.css('visibility', 'visible');
        //Hide navi-elements
        nav.each(function(index) {
            $(this).css('left', -500);
            $(this).css('opacity', '0.0');
        });
        //Hide feen
        feen.hide();

        /*animate-logo*/
        logo.animate({
            top : bounceHeight
        }, {
            duration : bounceDuration,
            easing : 'easeOutBounce'
        });

        /*animate-navi*/
        nav.each(function(index) {
            $(this).delay(index * (navFlyInSpeed + 300) + bounceDuration).animate({
                left : 0,
                opacity : 1.0
            }, {
                duration : navFlyInSpeed
            });
        });

        /*animate-feen*/
        feen.delay(nav.length * (navFlyInSpeed + 300) + bounceDuration).fadeIn(navFlyInSpeed, function() {
            // Animation complete
        });
    }

    /*All other pages*/
    else {
        //put the logo to the end-position without animation
        logo.css("top", bounceHeight);
        logo.css('visibility', 'visible');
    }

});
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
function mouseY(e) {
    if (!e)
        e = window.event;
    var body = (window.document.compatMode && window.document.compatMode == "CSS1Compat") ? window.document.documentElement : window.document.body;
    if (e.pageY) {
        var result = e.pageY;
    } else {
        var result = e.clientY + body.scrollTop - body.clientTop;
    }
    return result;
};
function mouseX(e) {
    if (!e)
        e = window.event;
    var body = (window.document.compatMode && window.document.compatMode == "CSS1Compat") ? window.document.documentElement : window.document.body;
    if (e.pageX) {
        var result = e.pageX;
    } else {
        var result = e.clientX + body.scrollLeft  - body.clientLeft;
    }
    return result;
};
