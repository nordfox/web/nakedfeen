/*
Source:
https://github.com/wei/ROT47/tree/e546f83c8ad5de48b8e819fa9ba1c2306d76b53f

License:
Copyright (c) 2016, Wei He

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

// https://rot47.net/
function rot47(x)
{var s=[];for(var i=0;i<x.length;i++)
{var j=x.charCodeAt(i);if((j>=33)&&(j<=126))
{s[i]=String.fromCharCode(33+((j+ 14)%94));}
else
{s[i]=String.fromCharCode(j);}}
return s.join('');}
