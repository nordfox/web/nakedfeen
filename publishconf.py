#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

PAGE_URL = '{slug}/'
ARTICLE_URL = '{slug}/'
CATEGORY_URL = 'category/{slug}/'

RELATIVE_URLS = False

DELETE_OUTPUT_DIRECTORY = True
