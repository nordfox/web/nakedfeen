Title: Indie meets Metal 😃
Date: 2015-03-30 14:38
Author: volker
Category: News
Slug: indie-meets-metal
Status: published

Hier eine kleine Interpretation des Songs "Masquerade" von unserer befreundeten Band [Hokum](http://www.hokum.de/). Die vorbildliche Metal-Version könnt ihr euch [hier](http://hokum.bandcamp.com/album/masquerade) anhören 😃 .  
Uns hat es einen riesen Spaß gemacht! Deshalb sagen wir herzlichst Danke für die tolle Inspiration einer genialen Band und wünschen euch jetzt viel Spaß mit Masquerade!

<iframe src="https://www.youtube.com/embed/ellEjp2l3WI" width="860" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
