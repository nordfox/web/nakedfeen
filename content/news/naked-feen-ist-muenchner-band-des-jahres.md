Title: NAKED FEEN ist Münchner Band des Jahres!
Date: 2015-03-30 14:39
Author: volker
Category: News
Slug: naked-feen-ist-muenchner-band-des-jahres
Status: published

Einfach unglaublich: Wir sind Münchner Band des Jahres und bekommen außerdem den heiß-begehrten Tour-Bus für ein ganzes Jahr!! Uns fehlen einfach die Worte - ihr habt unsere Gebete erhört und uns unseren größten Traum einer Deutschland-Tour ermöglicht! Wir können es selber noch gar nicht so richtig fassen, dass wir den Sprungbrett-Contest 2013 gewonnen haben. Ihr seid die Größten und wir sind euch zu tausend Dank verpflichtet.

Ebenfalls bedanken wir uns bei Puerto Nico, Like Time Flies und Meandering Mine - ihr wart großartig und wir freuen uns auf dem Theatron Sommerfestival nochmal gemeinsam mit euch zusammen zu spielen!

[![Sprungbrett]({static}../images/sprungbrett.jpg){: .image-process-article-image-border}]({static}../images/sprungbrett.jpg)


DANKE, WIR LIEBEN EUCH!!!  
Eure Nakeds
