Title: Neues Album "Heart Already Packed" erscheint am 23. Oktober!
Date: 2016-10-09 11:43
Author: volker
Category: News
Slug: neues-album-heart-already-packed-erscheint-am-23-oktober-und-ist-ab-sofort-vorberstellbar
Status: published

Juhu, unser neues Album "Heart Already Packed" erscheint am 23.10.2016 und ist ab sofort auf [iTunes](https://itunes.apple.com/de/album/heart-already-packed/id1163138634) vorbestellbar! Vorbesteller erhalten die Songs "Havana" und "Spacecat" auf iTunes schon jetzt.  
[![Heart Already Packed]({static}../images/heart-already-packged-front-quadratisch.jpg){: .image-process-article-image-border}]({static}../images/heart-already-packged-front-quadratisch.jpg)

