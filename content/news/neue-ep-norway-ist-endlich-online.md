Title: Neue EP Norway ist endlich online!
Date: 2013-05-29 14:36
Author: volker
Category: News
Slug: neue-ep-norway-ist-endlich-online
Status: published

Endlich! Wir haben Schweiß und Blut gelitten, um dieses kleine Meisterwerk zu vollbringen. Passend zum Wetter:

"NORWAY" die EP in voller Länge hier und jetzt.  
Viel Spaß beim hören! Zum Download gibt's die gesamte EP hier:  
[Norway auf Bandcamp](http://nakedfeen.bandcamp.com/album/norway)

DICKES MERCI an Christian Höck von den Telstar Studios, Matthias Heilig und alle Freunde dieser Welt!  
Unterstützt uns indem Ihr diesen Post teilt und ergattert euch damit auf ewig einen Platz im Naked Feen Band-Herzen 😃

Eure NF

<iframe src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F6360454" width="100%" height="400" frameborder="no" scrolling="no"></iframe>
