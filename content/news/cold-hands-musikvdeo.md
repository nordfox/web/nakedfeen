Title: Cold Hands Musikvdeo
Date: 2015-05-18 12:08
Author: volker
Category: News
Slug: cold-hands-musikvdeo
Status: published

Demian Grünzweig (Mufasa) hat bei einem Kurzfilm mitgemacht und fand die Bilder sehr passend zu einem alten Hit von uns, hier das Ergebnis seiner Arbeit. Viel Spaß damit!

<iframe src="https://www.youtube.com/embed/YUi9fyXSI-M" width="860" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
