Title: Neue Platte, neue Tour
Date: 2016-10-11 22:41
Author: volker
Category: News
Slug: neue-platte-neue-tour
Status: published

Während die Vögel nach Afrika fliegen, ziehen wir nach Norden durch die BRD.  
[![Tour Banner]({static}../images/tour-banner.jpg){: .image-process-article-image-border}]({static}../images/tour-banner.jpg)

Weitere Infos findet ihr unter [Dates]({filename}../pages/dates.md).
