Title: Neues Review zur neuen "Norway"-EP
Date: 2015-03-30 14:39
Author: volker
Category: News
Slug: neues-review-zur-neuen-norway-ep
Status: published

Schwuppdiwupp, und schon flattert das erste [Review](http://www.dontspreadbullshitspreadmusic.com/track-record-naked-feen-norway-ep/) zu unserer neuen "Norway"-EP herein.

[![Norwayy]({static}../images/norway-front.jpg){: .image-process-thumb}](http://www.dontspreadbullshitspreadmusic.com/track-record-naked-feen-norway-ep/)
> "Alternative Indie project Naked Feen bursts with personality and a unique sound that I can’t relate to any previous musical endeavor that I am aware of. With so many influences from Blues, Funk, Jazz, Indie, Pop, Rock, etc…. It’s a melting pot that births a unique yet reminiscent listening experience.
>
> New Sun, the closing track is the one that has sold me on re-listening to the album. With its more Norwegian approach to soundscaping, it’s exactly where I thought this band was going.
>
> Norway, the self-titled Ep track starts the album with such an amazing Ireland infused U2 rolling progression, that trademark percussive digital delay and the disco hats on drums. Naked Feen make seemingly overdone and mundane musical ideas novel and make them bloom again. This breath of fresh air sent into the lungs of Indie-rock has got me dancing, and for once, I am not ashamed."

DON'T SPREAD BULLSHIT, SPREAD MUSIC - wir fühlen uns geehrt und sagen Danke 😃
