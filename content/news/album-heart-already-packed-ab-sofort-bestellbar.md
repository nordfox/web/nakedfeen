Title: Album Heart Already Packed ab sofort bestellbar!
Date: 2016-10-23 18:29
Author: volker
Category: News
Slug: album-heart-already-packed-ab-sofort-bestellbar
Status: published

Endlich ist es soweit: Unser neues Album **Heart Already Packed** ist ab sofort in unserem neuen [Shop](https://www.nakedfeen.com/shop/) bestellbar. 😃 Außerdem auch ab sofort auf [iTunes](https://itunes.apple.com/de/album/heart-already-packed/id1163138634), [Amazon](https://www.amazon.de/dp/B01MA14RGR/ref=cm_sw_em_r_mt_dp_nzodybKCCSE33) und [Spotify](https://play.spotify.com/artist/67XOYpnCuWclwz7zR1YDEx) verfügbar.

![Heart Already Packed]({static}../images/heart-already-packged-front-quadratisch.jpg){: .image-process-article-image-border}


 
