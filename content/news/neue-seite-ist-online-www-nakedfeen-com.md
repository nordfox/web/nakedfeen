Title: Neue Seite ist online! www.nakedfeen.com
Date: 2013-05-27 14:22
Author: volker
Category: News
Slug: neue-seite-ist-online-www-nakedfeen-com
Status: published

Nach wochenlanger Arbeit gibt es nun endlich die lang ersehnte Homepage von NAKED FEEN! Wir bedanken uns an dieser Stelle bei all unseren fleißigen Unterstützern und ganz besonders bei unserem Thomas für seine Geduld, Inspiration und sein großes Können.

Viel Spaß auf der Seite!  
PS: Ein Shop usw. sind in Planung - es lohnt sich also immer wieder vorbei zu schauen.  
Eure NF
