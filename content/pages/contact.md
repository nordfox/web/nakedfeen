Title: Contact
Slug: contact
Status: published

## Impressum

**Naked Feen GbR**

<script language="JavaScript" type="text/javascript">
  function showContactInfo(element) {
    element.setAttribute("onclick", "");
    element.innerHTML = rot47("k255C6DDm'@=<6C y249Ek3Cm!F==:?86C \w2FAEDEC2ß6 `b2k3Cmgdbdc uC6:D:?8k3Cmk3Cm|2:=i k2 9C67lQ>2:=E@i:?7@o?2<65766?]4@>Qm:?7@o?2<65766?]4@>k^2mk^255C6DDm");
  };
</script>
<p><button onclick='showContactInfo(this.parentElement);' class="button">Kontaktdaten anzeigen</button></p>

Fotografen:  
Muriel Arnold [www.murielarnold.com](http://murielarnold.com/)  
Thomas Lietfien  

Haftungshinweis:  
Trotz sorgfältiger inhaltlicher Kontrolle wird keine Haftung für die Inhalte externer Links übernommen. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.

Alle Rechte vorbehalten.

Verantwortlich für Inhalt und Gestaltung der Internetpräsentation ist die Band Naked Feen.

Die Band Naked Feen erteilt die Erlaubnis alle auf diesen Internetseiten erscheinenden Inhalte zur Informationsgewinnung des Anwenders zu nutzen und Ausdrucke zu erstellen. Für eine gewerbliche Nutzung gilt dies nur nach einer vorher schriftlich erteilten Zustimmung.

Layout und Gestaltung dieser Präsentation sowie die enthaltenen Informationen sind gemäß dem Urheberrechtsgesetz geschützt. Das ist auch zu beachten, wenn auf diesen Internetseiten erscheinende Materialien Dritter zur Informationsgewinnung verwendet oder kopiert werden.

Eingetragene und nicht eingetragene Warenzeichen dieser Internetpräsentation oder Dritter dürfen ohne vorherige schriftliche Zustimmung nicht in Werbematerialien oder anderen Veröffentlichungen, im Zusammenhang mit der Verbreitung von Informationen verwendet werden.

Alle Angaben erfolgen ohne Gewähr. Eine Haftung für Schäden, die sich aus der Verwendung der veröffentlichten Inhalte ergeben, ist ausgeschlossen.

Diese Webseite wird veröffentlicht und gepflegt durch die Band Naked Feen.

Die Gestaltung und Programmierung wurde von Thomas König - [www.koenigthomas.de](http://www.koenigthomas.de "Thomas König") umgesetzt und von Naked Feen bearbeitet.
