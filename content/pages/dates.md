Title: Dates
Date: 2015-02-06 15:08
Author: volker
Category: Dates
Slug: dates
Status: published

## Coming soon

### 2016

10.12.16 - Album Release, Cord Club, München  
19.11.16 - Zeughaus, Passau  
04.11.16 - Katy's Garage, Dresden  
03.11.16 - Al Hamra, Berlin  
02.11.16 - Grüner Jäger, Hamburg  
31.10.16 - Reinraum, Düsseldorf  
29.10.16 - Blue-Shell, Köln  
22.10.16 - Neuhauser Musiknacht, Freiheiz, München  
<s>17.09.16 - Supporting The Animen, MUZ, Nürnberg</s>  
29.06.16 - TUNIX Festival Königsplatz, München  
11.06.16 - Wohnheim Chiemgaustraße Sommerfest, München  
26.05.16 - StuStaCulum Freimann, München  
06.02.16 - Biedersteiner Fasching, München

### 2015

14.11.15 - Kenial, Feierwerk, München  
22.10.15 - "Meine erste Studentenparty", StuSta, München  
12.09.15 - DingsDo Festival, Lajen Südtirol  
21.03.15 - Kunstmesse Stadtteilkultur 2411, München

### 2014

13.09.14 - Streetlife Festival Uniplatz, München  
12.09.14 - FSJ-Party Einstein-Kultur, München  
01.08.14 - Prima Leben und Stereo Festival, Freising  
26.07.14 - Sinnflut Festival, Erding  
25.07.14 - Kulturspektakel, Gauting  
23.07.14 - Cord Club, München  
17.07.14 - Stadt Land Rock Festival, Tollwood, München  
27.06.14 - TUNIX Festival Königsplatz, München  
09.04.14 - Supporting Listen to Polo, MUZ, Nürnberg  
06.03.14 - „Folløw Å.N.A goes unplugged", Substanz Club, München  
22.02.14 - Supporting Gil Ofarim & Acht, Eventhalle Westpark, Ingolstadt  
18.01.14 - Zeughaus, Passau

### 2013

28.12.13 - Munich Rocks! Ampere, München  
07.12.13 - Supporting Canvas Divinine, Live-Club, Bamberg  
08.11.13 - Supporting Young Chinese Dogs, Bogaloo Club, Pfarrkirchen  
26.10.13 - Neuhauser Musiknacht Freiheiz, München  
24.10.13 - Sage Club, Berlin  
23.10.13 - Grüner Jäger, Hamburg  
22.10.13 - JET, Leipzig  
15.10.13 - Puls BR, Bayerische Band der Woche, München  
12.10.13 - Digitalanalog Festival, München  
12.10.13 - 18.jetzt im Rathaus, München  
14.09.13 - Stadtschall-Festival, Schrobenhausen  
07.09.13 - Club Leonhard, Babensham  
15.08.13 - Filmfest, Weiterstadt bei Frankfurt  
11.08.13 - Theatron Sommerfestival, München  
20.07.13 - Live-Club, Bamberg  
13.07.13 - MINX Sommerfest, Würzburg  
06.07.13 - Sprungbrett, Feierwerk, München  
05.07.13 - Sommerfest - Evang. Studentenwohnheim, München  
02.07.13 - LMU Kunstausstellung, München  
22.06.13 - Sommerfest - Biedersteiner Wohnheim, München  
18.06.13 - Campus for Change - Glockenbachwerkstatt, München  
06.06.13 - Umsonst & Drinnen - E-Werk, Erlangen  
31.05.13 - StuStaCulum Freimann, München  
23.05.13 - Radio TopFM, Fürstenfeldbruck  
11.05.13 - Lange Nacht der Musik SUB, München  
10.05.13 - Live bei Radio M94.5, München  
03.05.13 - Sprungbrett, Feierwerk, München  
06.03.13 - Live bei Radio Lora, München  
30.03.13 - Frühlingsgefühle-Festival, Feierwerk, München  
14.02.13 - Sprungbrett, Feierwerk, München  
23.01.13 - 8Below Bergfest mit Blackout Problems & The Aberdeens, München  
22.01.13 - Campus for Change, Glockenbachwerkstatt, München

### 2012

06.12.12 - Nikolauskonzert, Glockenbachwerkstatt, München  
23.11.12 - on3-Südwild, La-Spezia-Platz, Bayreuth  
22.11.12 - Gooseberry Jam, Rausch & Töchter, München  
17.11.12 - "35mm meets Vinyl" Oberanger-Theater, München  
16.11.12 - Import Export, München  
12.10.12 - Stagetime Flowerstreet Records - Feierwerk, München  
05.10.12 - Schollwohnheim - "lauter als die Polizei erlaubt", München  
05.09.12 - Neckless-Tour, Garage Deluxe im Kunstpark, München  
25.08.12 - DingsDo Festival After Show Party, Lajen Südtirol  
09.08.12 - CORD Club, München  
02.08.12 - Free & Easy Festival im Backstage, München  
14.07.12 - Rocken & Poppen Festival im Feierwerk, München  
29.06.12 - "Bildungscamp" Geschwister-Scholl-Platz, München  
22.06.12 - "Wasserfest" Benefiz OpenAir Festival, Gröbenzell  
20.06.12 - "House of Music": Newcomer-Wettbewerb der MHMK im Backstage, München  
16.06.12 - Schollwohnheim Sommerfest, München  
16.06.12 - Wohnheim Biederstein Sommerfest, München  
16.06.12 - Wohnheim Chiemgaustraße Sommerfest, München  
07.06.12 - Campusfest HAW, Weiden  
26.05.12 - Radlnacht Odeonsplatz/Gärtnerplatz, München  
24.05.12 - Caritas Mädchenwohnheim, München  
10.05.12 - EP Release Party Substanz Club, München  
10.05.12 - Radio M94.5, München  
04.05.12 - ITF Beachtennis Party in der Beacharena, München  
03.05.12 - Stilrad, München  
02.05.12 - Radio Substanz "Up The Junction", München  
28.04.12 - Lange Nacht der Musik SUB, München  
24.04.12 - on3-Südwild Rathausplatz, Ingolstadt  
19.04.12 - egoFM Lokalhelden, München

### 2011

28.07.11 - Lupino, München  
17.06.11 - Massmann Sommerfest, München  
14.05.11 - Studentenstadt Freimann, München  
15.04.11 - Soundcafe, München

### 2010

21.12.10 - Schollwohnheim, München  
15.12.10 - Sauerbruch Wohnheim, München  
09.12.10 - Newman Wohnheim, München  
28.11.10 - Wohnheim Chiemgaustraße Sommerfest, München  
27.11.10 - Massmann Wohnheim, München  
23.07.10 - Sommerfest Michaeligymnasium, München
