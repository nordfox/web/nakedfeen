Title: Band
Date: 2015-02-06 15:08
Author: volker
Slug: band
Status: published


::: bandOverview id="bandOverview"

    [![Band - Boy]({static}../../images/band-boy.jpg){: .image-process-thumb}]({filename}boy.md)
    [![Band - Reverend]({static}../../images/band-reverend.jpg){: .image-process-thumb}]({filename}reverend.md)
    [![Band - Jazzman]({static}../../images/band-jazzman.jpg){: .image-process-thumb}]({filename}jazzman.md)
    [![Band - Mufasa]({static}../../images/band-mufasa.jpg){: .image-process-thumb}]({filename}mufasa.md)

„Soundheiß und haarig“ nennt sie das Curt Magazin und trifft damit genau ins Schwarze. Auch wenn die Band sich selbst gar nicht so haarig findet, heiß ist ihr Sound allemal. Und vereint dabei immer wieder melancholische Verträumtheit mit jugendlichem Leichtsinn. Die verspielten Gitarren erinnern an die Strokes, der Gesang - mal rau, mal gefühlvoll - schmiegt sich um eingängige Melodien und die treibenden Drums garnieren dieses feine Stück Indie-Rock mit der nötigen Brise Tanzbarkeit.

::: bandOverview id="bandOverview"
    [![Band - All]({static}../../images/band-all.jpg){: .image-process-article-image-border}]({static}../../images/band-all.jpg)
