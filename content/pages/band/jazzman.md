Title: Mr. Jazzman
Date: 2015-03-30 21:54
Author: volker
Slug: jazzman
Status: published

::: bandOverview id="bandOverview"

    [![Band - Jazzman]({static}../../images/band-jazzman.jpg){: .image-process-thumb}]({static}../../images/band-jazzman.jpg)
    [![Band - Jazzman]({static}../../images/band-jazzman.jpg){: .image-process-thumb}]({static}../../images/band-jazzman.jpg)
    [![Band - Jazzman]({static}../../images/band-jazzman.jpg){: .image-process-thumb}]({static}../../images/band-jazzman.jpg)
    [![Band - Jazzman]({static}../../images/band-jazzman.jpg){: .image-process-thumb}]({static}../../images/band-jazzman.jpg)

Mr. Jazzman ist ein netter Kerl, der gerne mal die Bandproben verschläft, obwohl der Proberaum nur 5 Minuten von seiner Wohnungstür entfernt ist. Neben seiner größten Leidenschaft bei Naked Feen den Frontman zu geben, versucht er Sport zu treiben, was aber durch sein fleißiges Gitarrespielen immer wieder vereitelt wird. Sein Lieblingsessen sind Gnocchi mit Pesto-Sahne-Soße. Seine momentane Lieblingsband ist Daughter.

[Zurück zur Band]({filename}band.md)
