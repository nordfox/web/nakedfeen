Title: Mufasa
Date: 2015-03-30 21:53
Author: volker
Slug: mufasa
Status: published

::: bandOverview id="bandOverview"

    [![Band - Mufasa]({static}../../images/band-mufasa.jpg){: .image-process-thumb}]({static}../../images/band-mufasa.jpg)
    [![Band - Mufasa]({static}../../images/band-mufasa.jpg){: .image-process-thumb}]({static}../../images/band-mufasa.jpg)
    [![Band - Mufasa]({static}../../images/band-mufasa.jpg){: .image-process-thumb}]({static}../../images/band-mufasa.jpg)
    [![Band - Mufasa]({static}../../images/band-mufasa.jpg){: .image-process-thumb}]({static}../../images/band-mufasa.jpg)

Mufasa ist allein schon dem Namen nach der Bandpapa. Er hat Naked Feen gegründet und hält die Jungs zusammen. Neben dem Trommeln für eine der besten Bands der Welt, liest er gerne mit Schulkindern den Hobbit und versucht ihnen zu erklären, dass es tatsächlich Spaß machen kann, Gedichte zu schreiben. Sein Kochspektrum ist häufig auf Nudeln mit Soße beschränkt. Seine momentane Lieblingsband ist Radiohead.

[Zurück zur Band]({filename}band.md)
