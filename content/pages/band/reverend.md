Title: The Reverend
Date: 2015-03-30 21:53
Author: volker
Slug: reverend
Status: published

::: bandOverview id="bandOverview"

    [![Band - Reverend]({static}../../images/band-reverend.jpg){: .image-process-thumb}]({static}../../images/band-reverend.jpg)
    [![Band - Reverend]({static}../../images/band-reverend.jpg){: .image-process-thumb}]({static}../../images/band-reverend.jpg)
    [![Band - Reverend]({static}../../images/band-reverend.jpg){: .image-process-thumb}]({static}../../images/band-reverend.jpg)
    [![Band - Reverend]({static}../../images/band-reverend.jpg){: .image-process-thumb}]({static}../../images/band-reverend.jpg)

Der Reverend ist der Jungspund von Naked Feen. Erst seit 2012 dabei, integriert und etabliert er sein Können auf vielseitigste Art und Weise. Musikalisch ist er ein talentierter Allrounder und auch, wenn es um technische Angelegenheiten geht, scheint es kein Problem zu sein, mal schnell eine Tischorgel oder den Verstärker von Mr. Jazzman zu zerlegen, um dem Problem auf den Zahn zu fühlen. Sein Lieblingsessen ist selbstgemachte Pizza mit viel Chilli. Seine momentane Lieblingsband ist Arcade Fire.

[Zurück zur Band]({filename}band.md)
