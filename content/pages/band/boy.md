Title: BOY!
Date: 2015-03-30 21:53
Author: volker
Slug: boy
Status: published
Category: band

::: bandOverview id="bandOverview"

    [![Band - Boy]({static}../../images/band-boy.jpg){: .image-process-thumb}]({static}../../images/band-boy.jpg)
    [![Band - Boy]({static}../../images/band-boy.jpg){: .image-process-thumb}]({static}../../images/band-boy.jpg)
    [![Band - Boy]({static}../../images/band-boy.jpg){: .image-process-thumb}]({static}../../images/band-boy.jpg)
    [![Band - Boy]({static}../../images/band-boy.jpg){: .image-process-thumb}]({static}../../images/band-boy.jpg)

BOY! ist ein Bandmitglied der ersten Stunde. Er prägt die Musik von Naked Feen durch seinen markanten und eigenwilligen Gitarrensound. Wenn er nicht den nächsten Hügel bebouldert, schreibt er am liebsten im stillen Kämmerchen seine Songs. Sein Lieblingsessen sind Asianudeln. Seine Lieblingsband ist Bloc Party.

[Zurück zur Band]({filename}band.md)
