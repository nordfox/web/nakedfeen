Title: Datenschutz
Slug: datenschutz
Status: published

## Welche Daten werden gesammelt

Daten sind dann am sichersten, wenn sie nicht gesammelt werden. Unsere Webseite verzichtet daher vollständig auf das Speichern von Besucherdaten, sowie den Einsatz von Werbung, Tracking-Diensten, Analysetools und Cookies.

Uns zugesandte E-Mails werden für die Zeit der Bearbeitung gespeichert.

## Eingebettete Inhalte von anderen Webseiten

Beiträge auf dieser Website können eingebettete Inhalte beinhalten (z.B. Videos, Bilder, Beiträge etc.). Eingebettete Inhalte von anderen Webseiten verhalten sich exakt so, als ob der Besucher die andere Website besucht hätte.

Diese Webseiten können Daten über den Besucher sammeln, Cookies benutzen, zusätzliche Tracking-Dienste von Dritten einbetten und Interaktion mit diesem eingebetteten Inhalt aufzeichnen, inklusive Interaktion mit dem eingebetteten Inhalt, falls ein persönliches Konto dort vorhanden ist.
