Title: Media
Date: 2015-02-06 15:08
Author: volker
Slug: media
Status: published

## Heart Already Packed 2016

Spacecat  
<iframe src="https://www.youtube.com/embed/VUqs86qy48s" allowfullscreen="allowfullscreen" width="853" height="480" frameborder="0"></iframe>

Havana  
<iframe src="https://www.youtube.com/embed/TYlM5AlnIbI" allowfullscreen="allowfullscreen" width="853" height="480" frameborder="0"></iframe>

## Norway 2013

<iframe src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F6360454" scrolling="no" width="100%" height="400" frameborder="no"></iframe>
Rodeo Girl  
<iframe src="https://www.youtube.com/embed/dgVLg668L-Y" allowfullscreen="allowfullscreen" width="860" height="480" frameborder="0"></iframe>

## Orange 2012

<iframe src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F6044042" scrolling="no" width="100%" height="500" frameborder="no"></iframe>  
Four Walls  
<iframe src="https://www.youtube.com/embed/O3jbjlVVlBc" allowfullscreen="allowfullscreen" width="860" height="480" frameborder="0"></iframe>

Masquerade  
<iframe src="https://www.youtube.com/embed/ellEjp2l3WI" allowfullscreen="allowfullscreen" width="860" height="480" frameborder="0"></iframe>

 

## Pictures
::: bandOverview id="bandOverview"
    [![T]({static}../images/presse-boy.jpg){: .image-process-thumb}]({static}../images/presse-boy.jpg)
    [![T]({static}../images/presse-jazzman.jpg){: .image-process-thumb}]({static}../images/presse-jazzman.jpg)
    [![T]({static}../images/presse-mufasa.jpg){: .image-process-thumb}]({static}../images/presse-mufasa.jpg)
    [![T]({static}../images/presse-reverend.jpg){: .image-process-thumb}]({static}../images/presse-reverend.jpg)

    [![T]({static}../images/kueche.jpg){: .image-process-thumb}]({static}../images/kueche.jpg)
    [![T]({static}../images/presse-theater.jpg){: .image-process-thumb}]({static}../images/presse-theater.jpg)
    [![T]({static}../images/naked-feen-wiese.jpg){: .image-process-thumb}]({static}../images/naked-feen-wiese.jpg)
    [![T]({static}../images/presse-fahrrad.jpg){: .image-process-thumb}]({static}../images/presse-fahrrad.jpg)

    [![T]({static}../images/plus-festival.jpg){: .image-process-thumb}]({static}../images/plus-festival.jpg)
    [![T]({static}../images/presse-neuhauser-musiknacht.jpg){: .image-process-thumb}]({static}../images/presse-neuhauser-musiknacht.jpg)
    [![T]({static}../images/presse-theatron.jpg){: .image-process-thumb}]({static}../images/presse-theatron.jpg)
    [![T]({static}../images/presse-oli.jpg){: .image-process-thumb}]({static}../images/presse-oli.jpg)



## Lyrics

::: lyrics
    ### Fire Flies

    ::: lyricsText
        She's still here  
        inside her heart  
        she won't move, she can't start  
        where's that light?  
        where's that spark?  
        That's meant to shine in the dark!

        So you  
        wanna do  
        something new  
        no one else has done  
        So you run  
        a 1000 miles till you get to the sun....

        And we'll be breaking away!

        Do you still think about it?  
        Do you still care?  
        Do you still worry about the faces you wear?  
        Are you alone? Are you afraid?  
        Do you still hang around these people you hate?

        So you  
        wanna do  
        something new  
        no one else has done  
        So you run  
        a 1000 miles till you get to the sun....

        But when the sun goes down we watch fireflies stray in the night  
        I see the world turn upside down in you're sparkling eyes  
        And when you here that sound  
        you know they never put you 6 feet under ground  
        I know it freaks you out  
        but we'll survive in this chaos, chaooos.

        And even though we watch the world falling apart  
        And even though we still chase daemons round our hearts  
        we celebrate the day.....

        When we were breaking away!


::: lyrics
    ### Lénine

    ::: lyricsText
        Come on and let me get it  
        I want to get it so much!  
        Come on and let me have it,  
        Cause I'm too tempted to touch!

        So come on, baby, show me that you know what I mean.  
        And there's no better way to do so, than being my rock'n'roll queen.

        Come on and let me show you  
        That the grass is still green.  
        Cause there'll be flowers in your garden  
        If you know what I mean.

        Un, deux, trois... What do you say?  
        Come on now and we'll be on our way!

        Cause baby you see,  
        that you could have been  
        Out in the green field with me  
        Where we could let it all just slide away.  
        But you - And I'm forgiving you -  
        Make me look like a fool.  
        You know that ain't so cool  
        But I keep trying cause there is no other way....


::: lyrics
    ### New Sun

    ::: lyricsText
        A cautious smile  
        just for a while  
        And if you try  
        it might take you by surprise

        Seasons keep changing around you  
        and seasons keep changing you  
        won't be the first and not the last in the line  
        don't be a fool running round wasting your time

        And in you dreams  
        you hear the walls fall down  
        but when you awake  
        you see a new sun rise up

        That's what it is that makes you love  
        That's what it is that makes you love

        Let it go now, just let it go now!  
        You won't be falling, falling!  
        Let it go now, just let it go now!  
        You won't be falling, falling!  
        Let it go now, just let it go now!  
        You won't be falling, falling!

        That's what it is that makes you love  
        That's what it is that makes you love


::: lyrics
    ### Norway

    ::: lyricsText
        A long lost ocean,  
        A flash into the night,  
        As you keep staring at the sky.  
        How far we've come now,  
        how far we've left the world behind  
        it's you and me into the wild.

        And I will slowly close my eyes  
        and face the silence  
        And I will slowly close my blinds  
        embrace the silence

        If only I could stay  
        forever and a day  
        I'd never run away  
        Surrendering our faith  
        and everything we chase  
        will slowly fall away.

        Am I asleep, am I awake?  
        Chasing shadows in the dark.  
        Have you seen the faces, they painted in your heart?  
        Have you seen what they show?  
        Have you been where I go?  
        As I'm melting away in an ocean of lights and snow

        And I will open up my eyes  
        and break the silence  
        And I will open up my blinds

        If only I could stay  
        forever and a day  
        I'd never run away  
        Surrendering our faith  
        and everything we chase  
        will slowly fall away.

        A broken bone in your back  
        you keep operating  
        You're never looking back  
        you keep operating  
        A broken bone in your back  
        you keep operating  
        You're never looking back...

        If only you could stay  
        forever and a day  
        You'd never run away  
        Surrendering our faith  
        and everything we chase  
        will slowly fall away.


::: lyrics
    ### Rodeo Girl

    ::: lyricsText
        I was born before the dawn  
        so the night became a part of me  
        You were born within a storm  
        so you think that's just the way to be

        But I don't wanna be another skull in your bag!  
        No I don't wanna be another skull in your bag!

        So why u wanna shoot me in the back of my head?  
        In the back of my head, of my head, of my head, oh no..

        Cause if you wanna leave me, then don't come back!  
        If you don't wanna please me, I say, there's the door, just go ahead!

        You were gone for far too long  
        still the night keeps creeping up on me.  
        I will sing a different song  
        than the one you used to sing to me.

        Cause I'm not gonna be another skull in your bag!  
        No, I'm not gonna be another skull in your bag!

        So you won't ever shoot me in the back of my head!  
        In the back of my head, of my head, of my head, oh no..

        Cause if you wanna leave me, then don't come back!  
        If you don't wanna please me, I say, there's the door, just go ahead!

        In the morning times it will shed a light  
        it will shed a light on me.  
        As the tides go up, the tides go down,  
        the tides will set me free

        Cause if you wanna leave me, then don't come back!  
        If you don't wanna please me, I say, there's the door, just go ahead!

        When you walk down the alley  
        the sun will shine again.  
        The rain will drown the valley.
