#!/usr/bin/env python
# -*- coding: utf-8 -*- #

PLUGINS = ['image_process']
MARKDOWN = {
    'extensions': ['md_in_html', 'customblocks', 'attr_list']
}

SITENAME = 'Naked Feen'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'de'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
THEME = './theme'

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

MENUITEMS_EXTRA = (
    ('News', 'category', 'news'),
    ('Dates', 'page', 'dates'),
    ('Band', 'page', 'band'),
    ('Media', 'page', 'media'),
    ('Naked', 'category', 'naked'),
    ('Contact', 'page', 'contact'),
    ('Datenschutz', 'page', 'datenschutz'),
)

BAND_MEMBERS = (
    ('BOY!', 'page', 'boy/'),
    ('The Reverend', 'page', 'reverend/'),
    ('Mr. Jazzman', 'page', 'jazzman/'),
    ('Mufasa', 'page', 'mufasa/'),
)

PAGE_URL = '{slug}/index.html'
PAGE_SAVE_AS = '{slug}/index.html'
ARTICLE_URL = '{slug}/index.html'
ARTICLE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = 'category/{slug}/index.html'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


IMAGE_PROCESS = {
    "article-image": ["scale_in 860 5000 False"],
    "article-image-border": ["scale_in 840 5000 False"],
    "thumb": ["scale_in 200 200 True"],
}

